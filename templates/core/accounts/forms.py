from django import forms
from core.accounts.models import Persona


class PersonaForm(forms.ModelForm):
    class Meta:
        model = Persona
        fields = '__all__'
