from django.contrib.auth import login
from rest_framework import status, viewsets
from rest_framework import generics
from rest_framework.response import Response
from rest_framework import permissions
from rest_framework.authtoken.serializers import AuthTokenSerializer
from knox.auth import TokenAuthentication
from knox.models import AuthToken
from knox.views import LoginView as KnoxLoginView
from core.accounts.models import Persona, Comuna
from core.accounts.serializers import ComunaSerializer, PersonaSerializer, UserSerializer, RegistroUserSerializer


# Create your views here.
class ComunaViewSet(viewsets.ModelViewSet):
    """EndPoint para las comunas, provee todos los metodos http, 
    requiere loguin con rest-knox

    Args:
        viewsets ([type]): [description]
    """
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = ComunaSerializer
    queryset = Comuna.objects.all()


class PersonViewSet(viewsets.ModelViewSet):
    """EndPoint para las personas, provee todos los metodos http, 
    requiere loguin con rest-knox, para la creación de una persona debe enviarse 
    el siguiente diccionario:
    {
        "user": 1, // id del usuario creado a través del endpoint register 
        "run": 12345678,
        "dv": "8",
        "nombres": "Margarita",
        "apePaterno": "Villalba",
        "direccion": "Los alerces",
        "numero": "611",
        "id_comuna": 2
    }

    Args:
        viewsets ([type]): [description]
    """
    authentication_classes = [TokenAuthentication, ]
    permission_classes = [permissions.IsAuthenticated, ]
    serializer_class = PersonaSerializer
    queryset = Persona.objects.all()


class LoginAPIView(KnoxLoginView):
    """EndPoint para realizar el login a la API
        requiere el envío los siguientes datos:
           {
                "username": "username",
                "email": "correo@gmail.com",
                "password": "password*"
            }
    Args:
        KnoxLoginView ([type]): [description]

    Returns:
        dict: usuario y token válidos
    """
    permission_classes = (permissions.AllowAny,)

    def post(self, request, format=None):
        serializer = AuthTokenSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data['user']
        login(request, user)
        return super(LoginAPIView, self).post(request, format=None)


class RegisterUserAPIView(generics.GenericAPIView):
    """EndPoint que permite registrar usuarios, sólo para login y testeo de API
        para el registro de usuarios y obtención del token de acceso 
        de deben enviar los siguientes datos:
        {
            "username":"username",
            "email": "correo@gmail.com",
            "password": "password"
        }
    Args:
        generics ([type]): [description]

    Returns:
        [type]: [description]
    """
    serializer_class = RegistroUserSerializer

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.save()
        return Response({
            "User": UserSerializer(user, context=self.get_serializer_context()).data,
            "Token": AuthToken.objects.create(user)[1]
        })
