from django.urls import path, include
from knox import views as knox_views
from core.accounts import views
from core.accounts.routers import router


urlpatterns = [
    # urls para autenticación de usuarios
    path('login/', views.LoginAPIView.as_view(), name='login'),
    path('logout/', knox_views.LogoutView.as_view(), name='logout'),
    path('logout_all/', knox_views.LogoutAllView.as_view(), name='logout_all'),
    path('register/', views.RegisterUserAPIView.as_view(), name='register'),

    # router para la api
    path('', include(router.urls)),
]
