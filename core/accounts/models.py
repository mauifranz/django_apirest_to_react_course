from django.db import models
from django.contrib.auth.models import User


# Create your models here.
class Comuna(models.Model):
    id = models.PositiveIntegerField(primary_key=True)
    nombre_comuna = models.CharField(max_length=150)

    def __str__(self):
        return self.nombre_comuna


class Persona(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    run = models.PositiveBigIntegerField(primary_key=True)
    dv = models.CharField(max_length=1)
    nombres = models.CharField(max_length=50)
    apePaterno = models.CharField(max_length=50)
    direccion = models.CharField(max_length=100)
    numero = models.CharField(max_length=15)
    id_comuna = models.ForeignKey(Comuna, on_delete=models.CASCADE)

    def __str__(self):
        return self.user.username
