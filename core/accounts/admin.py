from django.contrib import admin
from core.accounts.models import Comuna, Persona


# Register your models here.
admin.site.register(Comuna)
admin.site.register(Persona)
