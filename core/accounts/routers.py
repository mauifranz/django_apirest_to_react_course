from os import name
from rest_framework import routers
from core.accounts import views


router = routers.DefaultRouter()
router.register(r'personas', views.PersonViewSet, basename='personas')
router.register(r'comunas', views.ComunaViewSet, basename='comunas')
